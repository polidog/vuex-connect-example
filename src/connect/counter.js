import { connect } from 'vuex-connect'
import Counetr from '@/components/Counter'

export default connect({
  stateToProps: {
    count: state => state.count
  },
  methodsToEvents: {
    increment: ({ commit }, value) => commit('increment', value)
  }
})('counter', Counetr)
