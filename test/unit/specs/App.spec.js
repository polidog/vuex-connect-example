import { shallowMount } from '@vue/test-utils'
import App from '@/App'

describe('App', () => {
  // コンポーネントがマウントされ、ラッパが作成されます。
  const wrapper = shallowMount(App)

  // 要素の存在を確認することも簡単です
  it('has a button', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
